{-# Language OverloadedStrings #-}

import Network.HaskellNet.IMAP.SSL
import System.Environment
import System.Time
import Data.Text.Encoding (decodeUtf8)
import Codec.MIME.Parse (parseHeaders, parseMIMEType)
import Codec.MIME.Type (mimeType, MIMEParam (paramName, paramValue))
import qualified Data.Text as T

insertMessage params =
  
window :: TimeDiff
window = TimeDiff 0 0 0 0 (-10) 0 0

getHeaders conn uid =
     parseHeaders . decodeUtf8 <$> fetch conn uid

formatHeader hdr =
   T.intercalate ": " [paramName hdr, paramValue hdr] 

formatHeaders (params, rest) =
  T.unlines $ formatHeader <$> params

main = do
    let set = defaultSettingsIMAPSSL {
            sslDisableCertificateValidation = True,
            sslMaxLineLength=1048576
            }

    args <- getArgs

    let hostname = args!!0
    let user = args!!1
    let passwd = args!!2

    print "Connecting"
    con <- connectIMAPSSLWithSettings hostname set

    print "Authenticating"
    login con user passwd

    print "Selecting INBOX"
    select con "INBOX"

    now <- getClockTime
    initDate <- toCalendarTime $ addToClockTime window now

    print $ "Getting last " ++ show window ++ " of UIDs"
    ids <- search con [SENTSINCEs  initDate]

    print $ "Got " ++ (show . length $ ids) ++ "UIDs"

    hdrs <- mapM (getHeaders con) ids

    mapM_ putStrLn $ T.unpack . formatHeaders <$> hdrs
