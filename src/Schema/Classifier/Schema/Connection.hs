module Classifier.Schema.Connection where

import Database.HaskellDB.HDBC
import Database.HaskellDB.Sql.PostgreSQL
import Database.HDBC.PostgreSQL (connectPostgreSQL)

withDB opts = hdbcConnect generator (connectPostgreSQL conninfo)
  where conninfo = unwords [ k ++ "=" ++ v | (k,v) <- opts ]

opts = [("host","localhost")
       ,("user","evan")
       ,("dbname","email_dev")]
