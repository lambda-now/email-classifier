# Data Collection

The data will come from e-mail messages, as described by RFCs
[http://tools.ietf.org/html/rfc5322](5322) and
[http://tools.ietf.org/html/rfc6854](6854).

The bodies are described by the MIME RFCs:

* [http://tools.ietf.org/html/rfc2045](2045 - Multipurpose Internet Mail Extensions: Part I)
* [http://tools.ietf.org/html/rfc2184](2184 - MIME Parameter Value and Encoded Word Extensions)
* [http://tools.ietf.org/html/rfc2231](2231 - MIME Parameter Value and Encoded Word Extensions)
* [http://tools.ietf.org/html/rfc5335](5335 - Internationalized Email Headers)
* [http://tools.ietf.org/html/rfc6532](6532 - Internationalized Email Headers)

* [http://tools.ietf.org/html/rfc2046](2046 - Multipurpose Internet Mail Extensions: Part II)
* [http://tools.ietf.org/html/rfc2646](2646 - The Text/Plain Format Parameter)
* [http://tools.ietf.org/html/rfc3789](3676 - The Text/Plain Format and DelSp Parameters)


* [http://tools.ietf.org/html/rfc2289](2298 - An Extensible Message
  Format for Message Disposition Notifications)
* [http://tools.ietf.org/html/rfc3789](3798 - Message Disposition Notification)

* [http://tools.ietf.org/html/rfc3461](3461 - Simple Mail Transfer
  Protocol (SMTP) Service Extension for Delivery Status Notifications
  (DSNs))
* [http://tools.ietf.org/html/rfc3462](3462 -The Multipart/Report
  Content Type for the Reporting of Mail System Administrative
  Messages)
* [http://tools.ietf.org/html/rfc3885](3885 - SMTP Service Extension
  for Message Tracking)
* [http://tools.ietf.org/html/rfc3464](3464 - An Extensible Message
  Format for Delivery Status Notifications)
* [http://tools.ietf.org/html/rfc5337](5337 - Internationalized
  Delivery Status and Disposition Notifications)
* [http://tools.ietf.org/html/rfc6522](6522 - The Multipart/Report
  Media Type for the Reporting of Mail System Administrative Messages)
* [http://tools.ietf.org/html/rfc6533](6533 - Internationalized
  Delivery Status and Disposition Notifications)

* [http://tools.ietf.org/html/rfc5147](5147 - URI Fragment Identifiers
  for the text/plain Media Type)
* [http://tools.ietf.org/html/rfc6657](6657 - Update to MIME regarding
  "charset" Parameter Handling in Textual Media Types)

* [http://tools.ietf.org/html/rfc2047](2047 - Multipurpose Internet Mail Extensions: Part III)
* [http://tools.ietf.org/html/rfc2048](2048 - Multipurpose Internet Mail Extensions: Part IV)
* [http://tools.ietf.org/html/rfc2049](2049 - Multipurpose Internet Mail Extensions: Part V)
