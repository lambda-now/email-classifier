# Overview

This project will implement an e-mail classifier. It will learn from
your folders and start classifying e-mail as soon as it has enough
data to begin.

# Development

Clone the repository:

    git clone -b Development git://gitlab.com/lambda-now/email-classifier 

