create extension if not exists pg_trgm;

-- The container for an individual message.
create table message (
       -- From
       message_from varchar not null,

       -- Message-Id
       message_id varchar not null,

       -- Date, parsed with time zone info
       orig_date timestamp with time zone not null,

       -- Date this record was created.
       created_at timestamp with time zone not null default current_timestamp,

       primary key (message_from, message_id)
       );

create index message_from on message (message_from);
create index message_from_trgm on message using gist (message_from gist_trgm_ops);

create index message_id on message (message_id);
create index message_id_trgm on message using gist (message_id gist_trgm_ops);

create index message_created_at_idx on message (created_at);

-- The raw header text and hash.
create table headers (
       headers_text_sha512 varchar(128) not null,
       headers_text text not null,

       message_from varchar not null,
       message_id varchar not null,

       created_at timestamp with time zone not null default current_timestamp,

       primary key (headers_text_sha512),
       foreign key  (message_from, message_id)
               references message (message_from, message_id)
       );

create index headers_message_idx on headers (message_from, message_id);
create index headers_message_from_idx on headers (message_from);
create index headers_message_id_idx on headers (message_id);
create index headers_created_at_idx on headers (created_at);

-- An individual header, before additional parsing based on type.
create table header (
       headers_text_sha512 varchar(128) not null,
       header_name varchar not null,
       sequence_nbr bigint not null,
       contents text not null,
       created_at timestamp with time zone not null default current_timestamp,

       primary key (headers_text_sha512, header_name, sequence_nbr),
       foreign key (headers_text_sha512)
               references headers (headers_text_sha512)
       );

create index header_headers_text_sha512 on header (headers_text_sha512);

create index header_header_name_idx on header (header_name);
create index header_header_name_trgm on header using gist (header_name gist_trgm_ops);

create index header_sequence_nbr_idx on header (sequence_nbr);
create index header_created_at_idx on header (created_at);

create table body (
       body_text_sha512 varchar (128) not null,
       body_text text not null,

       message_from varchar not null,
       message_id varchar not null,

       created_at timestamp with time zone not null default current_timestamp,

       primary key (body_text_sha512),
       foreign key (message_from, message_id) references message (message_from, message_id)
       );

create index body_message_from_idx on body (message_from);
create index body_message_id_idx on body (message_id);
create index body_created_at_idx on body(created_at);
